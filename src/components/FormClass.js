import React, { Component } from 'react'
import ComponentsGroup from './componentGroup/'

export default class FormClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            txt:""
        }
    }
    handleTxt = (e) =>{
        this.setState({
            txt:e.target.value
        })
    }
    render() {
        const { Month } = ComponentsGroup
        return (
            <div className="form">
               <Month month={this.state.txt}/>
                <h3>Class Component</h3>
                <label htmlFor="">Wpisz Nazwe</label>
                <input name='name' value={this.state.txt} type="text" onChange={(e)=>this.handleTxt(e)}/>
            </div>
        )
    }
}


