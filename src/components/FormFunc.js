import React,{useState} from 'react'
import ComponentsGroup from './componentGroup'

const FormFunc = () => {
    const { Test } = ComponentsGroup
    const [ txt, setTxt ] = useState("")
    return (
        <div className="form">
        <Test {...{txt}}/>
            <h3>Func Component</h3>
            <label htmlFor="">Wpisz Nazwe</label>
            <input name='name' value={txt} type="text" onChange={(e)=>setTxt(e.target.value)}/>
        </div>
    )
}

export default FormFunc 
