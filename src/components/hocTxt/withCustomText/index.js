import CustomTxt from './CustomTxt'

const withCustomTxt = Component => props =>{
    return(
        <div>
            {
            props.txt === 'wywolaj' 
            ? <CustomTxt {...props}/>
            : <Component {...props}/>
            } 
        </div>
    )
}
export default withCustomTxt