import ComponentsGroup from '../componentGroup'
import Test from '../Test'
import withCustomText from './withCustomText'

ComponentsGroup.Test = withCustomText(props => <Test {...props} />)