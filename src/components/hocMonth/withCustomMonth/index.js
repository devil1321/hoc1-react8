import CustomMonth from './CustomMonth'

const withCustomMonth = Component => props =>{
    const date = new Date()
    const month = date.toLocaleString('default', { month: 'long' })
    return(
        <div>
            {
            props.month === month 
            ? <CustomMonth {...props}/>
            : <Component {...props}/>
            } 
        </div>
    )
}
export default withCustomMonth