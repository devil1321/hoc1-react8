import ComponentsGroup from '../componentGroup'
import Month from '../Month'
import withCustomMonth from './withCustomMonth'

ComponentsGroup.Month = withCustomMonth(props => <Month {...props} />)