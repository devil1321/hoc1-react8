import React from 'react'

const Months = (props) =>{
    return (
        <div>
            <h1>Wpisz aktualny miesiac</h1>
            <h2>{props.txt}</h2>
        </div>
    )
}

export default Months
