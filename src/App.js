import logo from './logo.svg';
import './App.css';
import FormFunc from './components/FormFunc'
import FormClass from './components/FormClass'
require('./components/hocTxt')
require('./components/hocMonth')
function App() {
  return (
    <div className="App">
      <FormFunc />
      <FormClass />
    </div>
  );
}

export default App;
